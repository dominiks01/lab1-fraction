#include <iostream>
#include <cstring>
#include <cctype>

using namespace std;

#include "fraction.h"

Fraction::Fraction():
   numerator_ {0}, denominator_ {1}
   {
   };

Fraction::Fraction(int i, int j=1):
   numerator_ {i}, denominator_ {j}
   {
   };

Fraction::Fraction(int i, int j, std::string name):
    numerator_ {i}, denominator_ {j}, fractionName_ {name}
   {
   };

int Fraction::removedFractions_ = 0;
int Fraction::invalidDenominatorValue = 0;


int Fraction::getNumerator() const {return this->numerator_;}
int Fraction::getDenominator() const {return this->denominator_;}

void Fraction::setNumerator(int value){this->numerator_ = value;};
void Fraction::setDenominator(int value){this->denominator_ = value;};

int Fraction::getInvalidDenominatorValue() {return invalidDenominatorValue;}

void Fraction::print() const {
    std::cout << numerator_ << "/" << denominator_ << "\n";
}

Fraction::~Fraction(){
    this->removedFractions_ += 1;
}

int Fraction::removedFractions(){
    return removedFractions_;
}

void Fraction::load(std::istream& is){
    int numerator, denominator;
    char delimiter;

    is >> numerator >> delimiter >> denominator;

    denominator_ = denominator;
    numerator_ = numerator;
}

void Fraction::save(std::ostream& os) const{
    os << numerator_ << "/" << denominator_;
}

std::string Fraction::getFractionName() const {
    return this->fractionName_;
}

